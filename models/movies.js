module.exports = (sequelize, type) => {
  const Movie = sequelize.define('movies',{
    id: {type: type.INTEGER, primaryKey:true, autoIncrement:true},
    title: {type: type.STRING, notNULL:true}
  });
  return Movie;
};
