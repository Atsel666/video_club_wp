const express = require('express');

//RESTFULL=>GET,POST,PUT,PATCH & DELETE
function list(req, res, next) {
  res.send('respond with a resource');
}

function index(req, res, next){
  res.send(`Usuario del sistema con ID:  ${req.params.id}`);
}

function create(req, res, next){
  const name = req.body.name;
  const lastName = req.body.lastName;
  res.send(`Crear un nuevo usuario con nombre ${name} y apellido ${lastName}`);
}

function replace(req, res, next){
  res.send("Reemplazo de usuario");
}

function edit(req, res, next){
  res.send(`Edito el usuario con ID = ${req.params.id} por otro`);
}

function destroy(req, res, next){
  res.send(`Elimino el usuario con ID = ${req.params.id}`);
}

module.exports = {
  list, index, create ,replace, edit, destroy
}
