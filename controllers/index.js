const express = require('express');

function home(req, res, next) {
  res.render('index', { title: 'Video club' });
}

module.exports = {
  home
}
