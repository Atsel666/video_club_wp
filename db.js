const Sequelize = require ('sequelize');
const directorModel = require('./models/director');
const genreModel = require('./models/genre');
const movieModel = require('./models/movies');
const actorModel = require('./models/actor');
const movieActorModel = require('./models/movieactor');
const copieModel = require('./models/copies');
const bookingModel = require('./models/booking');
const memberModel = require('./models/member');

//1) db name 2) user 3) password 4) Objeto conf
const sequelize = new Sequelize('video-club','root','abcd1234', {
  host:'localhost',//Direccion de nuestro RDBMS
  dialect:'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const MovieActor = movieActorModel(sequelize, Sequelize);
const Copie = copieModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Member = memberModel(sequelize,Sequelize);

//Modelos Base de datos:

// Un genero puede tener muchas peliculas
Genre.hasMany(Movie, {as:'movies'});
// Una pelicula puede tener un genero
Movie.belongsTo(Genre, {as:'genre'});


// Un director puede tener muchas peliculas
Director.hasMany(Movie, {as:'movies'});
// Una pelicula puede tener un director
Movie.belongsTo(Director, {as:'director'});


// Un actor participa en muchas peliculas
MovieActor.belongsTo(Movie, {foreignKey:'movieId'});
// En una pelicula participan muchos actores
MovieActor.belongsTo(Actor, {foreignKey:'actorId'});

Movie.belongsToMany(Actor, {
  foreignKey: 'actorId',
  as: 'actors',
  through: 'moviesActors'
});

Actor.belongsToMany(Movie, {
  foreignKey: 'movieId',
  as: 'movies',
  through: 'moviesActors'
});

// Una pelicula puede tener muchas copias
Movie.hasMany(Copie, {as:'copies'});
// Una copia puede tener una pelicula
Copie.belongsTo(Movie, {as:'movie'});

// Una copia puede tener muchos bookings
Copie.hasMany(Booking, {as:'bookings'});
// Un booking puede tener una copia
Booking.belongsTo(Copie, {as:'copy'});

// Una miembro puede tener muchos bookings
Member.hasMany(Booking, {as:'bookings'});
// Un booking puede tener una miembro
Booking.belongsTo(Member, {as:'member'});


sequelize.sync({
  force: true
}).then(()=>{
  console.log("Base de datos actualizada correctamente");
});

module.exports = {
  Director, Genre, Movie, Actor, MovieActor, Copie, Booking, Member
};
